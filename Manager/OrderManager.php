<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 23/05/16
 * Time: 10:15
 */

namespace eezeecommerce\OrderBundle\Manager;


use eezeecommerce\CartBundle\Cart\CartItem;
use eezeecommerce\CartBundle\Core\CartManager;
use eezeecommerce\CartBundle\Storage\StorageInterface;
use eezeecommerce\OrderBundle\Entity\Orders;
use eezeecommerce\OrderBundle\Event\OrderEvent;
use eezeecommerce\OrderBundle\Order\OrderItem;
use eezeecommerce\OrderBundle\OrderEvents;
use eezeecommerce\TaxBundle\Calculator\TaxCalculator;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class OrderManager implements OrderManagerInterface
{
    const ORDER = '_order';

    /**
     * @var EventDispatcherInterface
     */
    protected $dispatcher;

    /**
     * @var StorageInterface
     */
    protected $storage;

    /**
     * @var CurrencyItem
     */
    protected $currency;

    /**
     * @var TaxCalculator
     */
    protected $calculator;

    /**
     * @var CartManager
     */
    protected $cart;

    public function __construct(EventDispatcherInterface $dispatcher, StorageInterface $storage)
    {
        $this->storage = $storage;

        $this->dispatcher = $dispatcher;
    }

    /**
     * @param CartManager $cart
     */
    public function setCart(CartManager $cart)
    {
        $this->cart = $cart;
    }

    /**
     * @return CartManager
     */
    public function getCart()
    {
        return $this->cart;
    }

    /**
     * Add Item To Cart
     *
     * @param CartItem $item
     * @param int $quantity
     *
     * @return mixed
     */
    public function setOrder(Orders $order)
    {
        $item = new OrderItem($order);

        $event = new OrderEvent($item);

        $this->dispatcher->dispatch(OrderEvents::ORDER_SAVE_INITIALISED, $event);

        if ($event->isPropagationStopped()) {
            return;
        }

        $this->storage->set(SELF::ORDER, $item);

        $this->dispatcher->dispatch(OrderEvents::ORDER_SAVE_COMPLETED, $event);
    }

    /**
     * Check if order exists
     *
     * @param $itemOrId
     *
     * @return mixed
     */
    public function hasOrder()
    {
        return $this->getOrder() ? true : false;
    }

    /**
     * Get order from storage
     *
     * @return OrderItem
     */
    public function getOrder()
    {
        return $this->storage->get(SELF::ORDER);
    }

    /**
     * Count number of order in storage
     *
     * @return mixed
     */
    public function count()
    {
        return count($this->getOrder());
    }

    /**
     * Check if order is empty
     *
     * @return boolean
     */
    public function isEmpty()
    {
        return ($this->count() === 0);
    }

    /**
     * Clear order
     *
     * @return mixed
     */
    public function clear()
    {
        $event = new OrderEvent($this->getOrder());

        $this->dispatcher->dispatch(OrderEvents::ORDER_REMOVE_INITIALISED, $event);

        if ($event->isPropagationStopped()) {
            return;
        }

        $this->storage->remove(SELF::ORDER);

        $this->dispatcher->dispatch(OrderEvents::ORDER_REMOVE_COMPLETED, $event);
    }

    public function getTax()
    {
        $orderEntity = $this->getOrder()->getEntity();
        $shipping = $orderEntity->getShippingAddress()->getCountry();

        $cart = $this->getCart();

        $amount = $cart->total() - $cart->getDiscount();


        return $this->calculator->calculate($amount, $shipping->getTaxRate());
    }

    public function setCalculator(TaxCalculator $calculator)
    {
        $this->calculator = $calculator;
    }
}