<?php

namespace eezeecommerce\OrderBundle\Manager;


use eezeecommerce\CartBundle\Cart\CartItem;
use eezeecommerce\OrderBundle\Entity\Orders;

interface OrderManagerInterface
{
    /**
     * Add Item To Cart
     *
     * @param CartItem $item
     * @param int $quantity
     *
     * @return mixed
     */
    public function setOrder(Orders $order);

    /**
     * Check if order exists
     *
     * @param $itemOrId
     *
     * @return mixed
     */
    public function hasOrder();

    /**
     * Get order from storage
     *
     * @return mixed
     */
    public function getOrder();

    /**
     * Count number of order in storage
     *
     * @return mixed
     */
    public function count();

    /**
     * Check if order is empty
     *
     * @return boolean
     */
    public function isEmpty();

    /**
     * Clear order
     *
     * @return mixed
     */
    public function clear();
}