<?php

namespace eezeecommerce\OrderBundle\Manager;

use Doctrine\Common\Persistence\ObjectManager;
use eezeecommerce\CartBundle\Core\CartManager;
use eezeecommerce\OrderBundle\Entity\OrderLines;
use eezeecommerce\OrderBundle\Entity\Orders;
use eezeecommerce\ShippingBundle\Entity\Country;
use eezeecommerce\UserBundle\Entity\User;
use eezeecommerce\OrderBundle\Entity\OptionsOrderlines;

class OrderProcessor
{
    /**
     * @var ObjectManager
     */
    private $om;

    /**
     * @var Orders
     */
    private $order;

    /**
     * @var CartManager
     */
    private $cart;

    /**
     * @var User|null
     */
    private $user;

    public function __construct(ObjectManager $om, CartManager $cart)
    {
        $this->om = $om;
        $this->cart = $cart;
    }

    public function getOrder()
    {
        return $this->order;
    }

    public function setOrder(Orders $order)
    {
        $this->order = $order;
    }

    public function setUser($user)
    {
        $this->user = $user;
    }

    public function prepareOrder()
    {
        $this->processAddress();
        $this->om->clear();

        return $this->order;
    }

    private function processAddress()
    {
        if (null !== $user = $this->user) {
            $this->order->getBillingAddress()->setUser($user);
            $this->order->getShippingAddress()->setUser($user);
            $this->order->setUser($user);
        }
        $this->setBillingAddress();
        $this->setShippingAddress();
        $this->setDetails();
        $this->order->setOrderNumber($this->order->getId() + 10000);
        $this->order->setOrderDate(new \DateTime());
        $this->om->persist($this->order);
        $this->om->flush();
    }

    private function setBillingAddress()
    {
        $country = $this->om->merge($this->order->getBillingAddress()->getCountry());
        $this->order->getBillingAddress()->setCountry($country);
    }

    private function setShippingAddress()
    {
        if ($this->order->getShippingAddress()->getCountry() instanceof Country) {
            $country = $this->om->merge($this->order->getShippingAddress()->getCountry());
        } else {
            $country = $this->om->merge($this->order->getBillingAddress()->getCountry());
        }
        $this->order->getShippingAddress()->setCountry($country);
    }

    private function setDetails()
    {
        $this->order->setExchangeRate($this->cart->getCurrency()->getEntity()->getExchangeRate());
        $this->order->setCurrencyCode($this->cart->getCurrencyCode());
        $this->order->setDiscountTotal($this->cart->getDiscount());
    }

    public function process(Orders $order)
    {
        $this->addOrderLines($order);
    }

    private function addOrderLines(Orders $order)
    {
        foreach ($this->cart->getItems() as $item) {
            $order = $this->om->getRepository(Orders::class)
                ->find($order->getId());
            $orderLine = new OrderLines();
            $stock = $this->om->merge($item->getEntity()->getStock());
            $product = $this->om->merge($item->getEntity());
            $product->setStock($stock);
            if (null !== $item->getChosenVariant()) {
                $variant = $this->om->merge($item->getChosenVariant());
                $orderLine->setVariant($variant);
            }
            $orderLine->setOrder($order);
            $orderLine->setProductAmount($item->getPrice());
            $orderLine->setTotalAmount($item->getSubTotal());
            $orderLine->setQty($item->getQuantity());
            $orderLine->setProduct($product);
            $this->om->persist($orderLine);
            $this->addOptions($item->getOptions(), $orderLine);
            $this->om->flush();
            $this->om->clear();
        }
        $this->om->flush();

    }

    private function addOptions($options, OrderLines $orderLine)
    {

        foreach ($options as $o) {
            if (null !== $o->getValue()) {
                $option = $this->om->merge($o);
                $optionsOrderLine = new OptionsOrderlines();
                $optionsOrderLine->setOptions($option);
                if ($option->getType() === "file") {
                    $file = $this->om->getRepository("eezeecommerceUploadBundle:Uploader")
                        ->findOneBy(array("imageName" => $option->getValue()));
                    if (null !== $file) {
                        $optionsOrderLine->addFile($file);
                    }
                }
                $optionsOrderLine->setOptionAmount($o->getBasePrice());
                if ($option->getType() === "array") {
                    if ($option->getOptionChoices()->count() > 0) {
                        foreach ($option->getOptionChoices() as $choice) {
                            if ($option->getValue() == $choice->getName()) {
                                $optionChoice = $this->om->merge($choice);
                                $optionsOrderLine->setOptionChoice($optionChoice);
                                if ($optionChoice->getBasePrice() !== 0.0000) {
                                    $optionsOrderLine->setOptionAmount($optionChoice->getBasePrice());
                                }
                            }
                        }
                    }
                }
                $optionsOrderLine->setValue($option->getValue());

                if (null === $orderLine->getId()) {
                    $orderLine = $this->om->merge($orderLine);
                }
                if (null === $orderLine->getId()) {
                    $this->om->persist($orderLine);
                    $this->om->flush();
                }
                $this->om->persist($optionsOrderLine);

                $orderLine->addOption($optionsOrderLine);
                $this->om->persist($orderLine);
                $this->om->flush();
            }
        }
    }
}
