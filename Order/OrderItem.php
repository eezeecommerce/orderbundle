<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 23/05/16
 * Time: 09:51
 */

namespace eezeecommerce\OrderBundle\Order;


use eezeecommerce\OrderBundle\Entity\Orders;

class OrderItem
{
    protected $order;

    public function __construct(Orders $order)
    {
        $this->order = $order;
    }

    public function getEntity()
    {
        return $this->order;
    }
}