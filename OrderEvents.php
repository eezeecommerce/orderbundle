<?php

/*
 * Developed by EezeeCommerce
 * All rights reserved and subject to copyright.
 * https://www.eezeecommerce.com
 */
namespace eezeecommerce\OrderBundle;

/**
 * OrderEvents
 *
 * @author root
 */
final class OrderEvents
{
    const ORDER_SAVE_INITIALISED = "eezeecommerce.order.save_initialised";

    const ORDER_SAVE_COMPLETED = "eezeecommerce.order.save_completed";

    const ORDER_REMOVE_INITIALISED = "eezeecommerce.order.remove_initialised";

    const ORDER_REMOVE_COMPLETED = "eezeecommerce.order.remove_completed";
}
