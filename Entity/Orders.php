<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace eezeecommerce\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="orders")
 * @ORM\Entity(repositoryClass="eezeecommerce\OrderBundle\Entity\OrdersRepository")
 */
class Orders
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    protected $vat_number;
    /**
     * @ORM\Column(type="string", length=50)
     */
    protected $order_number = 0;
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $order_date;
    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    protected $courier_name;
    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    protected $courier_service;
    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    protected $est_delivery;
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $courier_delivery_min;
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $courier_delivery_max;
    /**
     * @ORM\Column(type="decimal", precision=10, scale=4)
     */
    protected $shipping_total = 0.0000;
    /**
     * @ORM\Column(type="decimal", precision=10, scale=4)
     */
    protected $subtotal = 0.0000;
    /**
     * @ORM\Column(type="decimal", precision=10, scale=4)
     */
    protected $total = 0.0000;
    /**
     * @ORM\Column(type="decimal", precision=10, scale=4)
     */
    protected $discount_total = 0.0000;
    /**
     * @ORM\Column(type="decimal", precision=10, scale=4)
     */
    protected $tax_total = 0.0000;
    /**
     * @ORM\Column(type="decimal", precision=10, scale=4)
     */
    protected $tax_exempt_total = 0.0000;
    /**
     * @ORM\Column(type="decimal", precision=10, scale=4)
     */
    protected $total_weight = 0.0000;
    /**
     * @ORM\Column(type="decimal", precision=10, scale=4)
     */
    protected $total_vol_weight = 0.0000;
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $notes;
    /**
     * @ORM\Column(type="boolean")
     */
    protected $allows_marketing = false;
    /**
     * @ORM\Column(type="boolean")
     */
    protected $fulfilled = false;
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fulfilled_ts;
    /**
     * @ORM\Column(type="boolean")
     */
    protected $deleted = false;
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deleted_ts;
    /**
     * @ORM\Column(type="boolean")
     */
    protected $shipped = false;
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $shipped_ts;
    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('none', 'green', 'blue', 'orange', 'purple', 'red', 'yellow', 'gray')", nullable=true)
     */
    protected $flag = "none";
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $currency_code;
    /**
     * @ORM\Column(type="decimal", precision=19, scale=6, nullable=true)
     */
    protected $exchange_rate;
    /**
     * @ORM\OneToMany(targetEntity="\eezeecommerce\PaymentBundle\Entity\PaymentDetails", mappedBy="order")
     * @ORM\JoinColumn(name="payment_id", referencedColumnName="order")
     * @ORM\OrderBy({"ts" = "DESC"})
     */
    private $payment;
    /**
     * @ORM\OneToMany(targetEntity="OrderLines", mappedBy="order")
     */
    private $order_lines;
    /**
     * @ORM\ManyToOne(targetEntity="\eezeecommerce\UserBundle\Entity\User", inversedBy="orders")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;
    /**
     * @Assert\Valid
     * @ORM\ManyToOne(targetEntity="eezeecommerce\UserBundle\Entity\Address", inversedBy="orders_billing_address", cascade={"persist"})
     * @ORM\JoinColumn(name="billing_address_id", referencedColumnName="id")
     */
    private $billing_address;

    /**
     * @Assert\Valid
     * @ORM\ManyToOne(targetEntity="eezeecommerce\UserBundle\Entity\Address", inversedBy="orders_shipping_address", cascade={"persist"})
     * @ORM\JoinColumn(name="shipping_address_id", referencedColumnName="id")
     */
    private $shipping_address;

    /**
     * @ORM\OneToMany(targetEntity="OrderNotes", mappedBy="order")
     */
    private $order_notes;


    /**
     * Orders constructor.
     */
    public function __construct()
    {
        $this->orders_lines = new ArrayCollection();
        $this->setOrderDate(new \DateTime());
        $this->order_notes = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get orderNumber
     *
     * @return string
     */
    public function getOrderNumber()
    {
        return $this->order_number;
    }

    /**
     * Set orderNumber
     *
     * @param string $orderNumber
     *
     * @return Orders
     */
    public function setOrderNumber($orderNumber)
    {
        $this->order_number = $orderNumber;

        return $this;
    }

    /**
     * Get orderDate
     *
     * @return \DateTime
     */
    public function getOrderDate()
    {
        return $this->order_date;
    }

    /**
     * Set orderDate
     *
     * @param \DateTime $orderDate
     *
     * @return Orders
     */
    public function setOrderDate($orderDate)
    {
        $this->order_date = $orderDate;

        return $this;
    }

    /**
     * Get courierName
     *
     * @return string
     */
    public function getCourierName()
    {
        return $this->courier_name;
    }

    /**
     * Set courierName
     *
     * @param string $courierName
     *
     * @return Orders
     */
    public function setCourierName($courierName)
    {
        $this->courier_name = $courierName;

        return $this;
    }

    /**
     * Get courierService
     *
     * @return string
     */
    public function getCourierService()
    {
        return $this->courier_service;
    }

    /**
     * Set courierService
     *
     * @param string $courierService
     *
     * @return Orders
     */
    public function setCourierService($courierService)
    {
        $this->courier_service = $courierService;

        return $this;
    }

    /**
     * Get estDelivery
     *
     * @return string
     */
    public function getEstDelivery()
    {
        return $this->est_delivery;
    }

    /**
     * Set estDelivery
     *
     * @param string $estDelivery
     *
     * @return Orders
     */
    public function setEstDelivery($estDelivery)
    {
        $this->est_delivery = $estDelivery;

        return $this;
    }

    /**
     * Get shippingTotal
     *
     * @return integer
     */
    public function getShippingTotal()
    {
        return $this->shipping_total;
    }

    /**
     * Set shippingTotal
     *
     * @param integer $shippingTotal
     *
     * @return Orders
     */
    public function setShippingTotal($shippingTotal)
    {
        $this->shipping_total = $shippingTotal;

        return $this;
    }

    /**
     * Get subtotal
     *
     * @return integer
     */
    public function getSubtotal()
    {
        return $this->subtotal;
    }

    /**
     * Set subtotal
     *
     * @param integer $subtotal
     *
     * @return Orders
     */
    public function setSubtotal($subtotal)
    {
        $this->subtotal = $subtotal;

        return $this;
    }

    /**
     * Get total
     *
     * @return integer
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set total
     *
     * @param integer $total
     *
     * @return Orders
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get discountTotal
     *
     * @return integer
     */
    public function getDiscountTotal()
    {
        return $this->discount_total;
    }

    /**
     * Set discountTotal
     *
     * @param integer $discountTotal
     *
     * @return Orders
     */
    public function setDiscountTotal($discountTotal)
    {
        $this->discount_total = $discountTotal;

        return $this;
    }

    /**
     * Get taxTotal
     *
     * @return integer
     */
    public function getTaxTotal()
    {
        return $this->tax_total;
    }

    /**
     * Set taxTotal
     *
     * @param integer $taxTotal
     *
     * @return Orders
     */
    public function setTaxTotal($taxTotal)
    {
        $this->tax_total = $taxTotal;

        return $this;
    }

    /**
     * Get totalWeight
     *
     * @return integer
     */
    public function getTotalWeight()
    {
        return $this->total_weight;
    }

    /**
     * Set totalWeight
     *
     * @param integer $totalWeight
     *
     * @return Orders
     */
    public function setTotalWeight($totalWeight)
    {
        $this->total_weight = $totalWeight;

        return $this;
    }

    /**
     * Get totalVolWeight
     *
     * @return integer
     */
    public function getTotalVolWeight()
    {
        return $this->total_vol_weight;
    }

    /**
     * Set totalVolWeight
     *
     * @param integer $totalVolWeight
     *
     * @return Orders
     */
    public function setTotalVolWeight($totalVolWeight)
    {
        $this->total_vol_weight = $totalVolWeight;

        return $this;
    }

    /**
     * Get notes
     *
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set notes
     *
     * @param string $notes
     *
     * @return Orders
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get fulfilled
     *
     * @return boolean
     */
    public function getFulfilled()
    {
        return $this->fulfilled;
    }

    /**
     * Set fulfilled
     *
     * @param boolean $fulfilled
     *
     * @return Orders
     */
    public function setFulfilled($fulfilled)
    {
        $this->fulfilled = $fulfilled;

        return $this;
    }

    /**
     * Get fulfilledTs
     *
     * @return \DateTime
     */
    public function getFulfilledTs()
    {
        return $this->fulfilled_ts;
    }

    /**
     * Set fulfilledTs
     *
     * @param \DateTime $fulfilledTs
     *
     * @return Orders
     */
    public function setFulfilledTs($fulfilledTs)
    {
        $this->fulfilled_ts = $fulfilledTs;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return Orders
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
        $this->setDeletedTs(new \DateTime());

        return $this;
    }

    /**
     * Get deletedTs
     *
     * @return \DateTime
     */
    public function getDeletedTs()
    {
        return $this->deleted_ts;
    }

    /**
     * Set deletedTs
     *
     * @param \DateTime $deletedTs
     *
     * @return Orders
     */
    public function setDeletedTs($deletedTs)
    {
        $this->deleted_ts = $deletedTs;

        return $this;
    }

    /**
     * Get shipped
     *
     * @return boolean
     */
    public function getShipped()
    {
        return $this->shipped;
    }

    /**
     * Set shipped
     *
     * @param boolean $shipped
     *
     * @return Orders
     */
    public function setShipped($shipped)
    {
        $this->setShippedTs(new \DateTime());

        $this->shipped = $shipped;

        return $this;
    }

    /**
     * Get shippedTs
     *
     * @return \DateTime
     */
    public function getShippedTs()
    {
        return $this->shipped_ts;
    }

    /**
     * Set shippedTs
     *
     * @param \DateTime $shippedTs
     *
     * @return Orders
     */
    public function setShippedTs($shippedTs)
    {
        $this->shipped_ts = $shippedTs;

        return $this;
    }

    /**
     * Add payment
     *
     * @param \eezeecommerce\PaymentBundle\Entity\Payment $payment
     *
     * @return Orders
     */
    public function addPayment(\eezeecommerce\PaymentBundle\Entity\Payment $payment)
    {
        $this->payment[] = $payment;

        return $this;
    }

    /**
     * Remove payment
     *
     * @param \eezeecommerce\PaymentBundle\Entity\Payment $payment
     */
    public function removePayment(\eezeecommerce\PaymentBundle\Entity\Payment $payment)
    {
        $this->payment->removeElement($payment);
    }

    /**
     * Get payment
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * Add OrderLines
     *
     * @param OrderLines $orderLine
     *
     * @return Orders
     */
    public function addOrderLine(OrderLines $orderLine)
    {
        $this->order_lines[] = $orderLine;

        return $this;
    }

    /**
     * Remove orderLine
     *
     * @param Orderlines $orderLine
     */
    public function removeOrderLine(OrderLines $orderLine)
    {
        $this->order_lines->removeElement($orderLine);
    }

    /**
     * Get orderLines
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrderLines()
    {
        return $this->order_lines;
    }

    /**
     * Get user
     *
     * @return \eezeecommerce\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set user
     *
     * @param \eezeecommerce\UserBundle\Entity\User $user
     *
     * @return Orders
     */
    public function setUser(\eezeecommerce\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get allowsMarketing
     *
     * @return boolean
     */
    public function getAllowsMarketing()
    {
        return $this->allows_marketing;
    }

    /**
     * Set allowsMarketing
     *
     * @param boolean $allowsMarketing
     *
     * @return Orders
     */
    public function setAllowsMarketing($allowsMarketing)
    {
        $this->allows_marketing = $allowsMarketing;

        return $this;
    }

    /**
     * Get taxExemptTotal
     *
     * @return string
     */
    public function getTaxExemptTotal()
    {
        return $this->tax_exempt_total;
    }

    /**
     * Set taxExemptTotal
     *
     * @param string $taxExemptTotal
     *
     * @return Orders
     */
    public function setTaxExemptTotal($taxExemptTotal)
    {
        $this->tax_exempt_total = $taxExemptTotal;

        return $this;
    }

    /**
     * Get vatNumber
     *
     * @return string
     */
    public function getVatNumber()
    {
        return $this->vat_number;
    }

    /**
     * Set vatNumber
     *
     * @param string $vatNumber
     *
     * @return Orders
     */
    public function setVatNumber($vatNumber)
    {
        $this->vat_number = $vatNumber;

        return $this;
    }

    /**
     * Get flag
     *
     * @return string
     */
    public function getFlag()
    {
        return $this->flag;
    }

    /**
     * Set flag
     *
     * @param string $flag
     *
     * @return Orders
     */
    public function setFlag($flag)
    {
        $this->flag = $flag;

        return $this;
    }

    /**
     * Get billingAddress
     *
     * @return \eezeecommerce\UserBundle\Entity\Address
     */
    public function getBillingAddress()
    {
        return $this->billing_address;
    }

    /**
     * Set billingAddress
     *
     * @param \eezeecommerce\UserBundle\Entity\Address $billingAddress
     *
     * @return Orders
     */
    public function setBillingAddress(\eezeecommerce\UserBundle\Entity\Address $billingAddress = null)
    {
        $this->billing_address = $billingAddress;

        return $this;
    }

    /**
     * Get shippingAddress
     *
     * @return \eezeecommerce\UserBundle\Entity\Address
     */
    public function getShippingAddress()
    {
        return $this->shipping_address;
    }

    /**
     * Set shippingAddress
     *
     * @param \eezeecommerce\UserBundle\Entity\Address $shippingAddress
     *
     * @return Orders
     */
    public function setShippingAddress(\eezeecommerce\UserBundle\Entity\Address $shippingAddress = null)
    {
        $this->shipping_address = $shippingAddress;

        return $this;
    }

    /**
     * Get currencyCode
     *
     * @return string
     */
    public function getCurrencyCode()
    {
        return $this->currency_code;
    }

    /**
     * Set currencyCode
     *
     * @param string $currencyCode
     *
     * @return Orders
     */
    public function setCurrencyCode($currencyCode)
    {
        $this->currency_code = $currencyCode;

        return $this;
    }

    /**
     * Get exchangeRate
     *
     * @return string
     */
    public function getExchangeRate()
    {
        return $this->exchange_rate;
    }

    /**
     * Set exchangeRate
     *
     * @param string $exchangeRate
     *
     * @return Orders
     */
    public function setExchangeRate($exchangeRate)
    {
        $this->exchange_rate = $exchangeRate;

        return $this;
    }

    /**
     * Get courierDeliveryMin
     *
     * @return integer
     */
    public function getCourierDeliveryMin()
    {
        return $this->courier_delivery_min;
    }

    /**
     * Set courierDeliveryMin
     *
     * @param integer $courierDeliveryMin
     *
     * @return Orders
     */
    public function setCourierDeliveryMin($courierDeliveryMin)
    {
        $this->courier_delivery_min = $courierDeliveryMin;

        return $this;
    }

    /**
     * Get courierDeliveryMax
     *
     * @return integer
     */
    public function getCourierDeliveryMax()
    {
        return $this->courier_delivery_max;
    }

    /**
     * Set courierDeliveryMax
     *
     * @param integer $courierDeliveryMax
     *
     * @return Orders
     */
    public function setCourierDeliveryMax($courierDeliveryMax)
    {
        $this->courier_delivery_max = $courierDeliveryMax;

        return $this;
    }

    /**
     * Add orderNote
     *
     * @param \eezeecommerce\OrderBundle\Entity\OrderNotes $orderNote
     *
     * @return Orders
     */
    public function addOrderNote(\eezeecommerce\OrderBundle\Entity\OrderNotes $orderNote)
    {
        $this->order_notes[] = $orderNote;

        return $this;
    }

    /**
     * Remove orderNote
     *
     * @param \eezeecommerce\OrderBundle\Entity\OrderNotes $orderNote
     */
    public function removeOrderNote(\eezeecommerce\OrderBundle\Entity\OrderNotes $orderNote)
    {
        $this->order_notes->removeElement($orderNote);
    }

    /**
     * Get orderNotes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrderNotes()
    {
        return $this->order_notes;
    }
}
