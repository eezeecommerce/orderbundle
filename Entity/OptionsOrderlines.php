<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace eezeecommerce\OrderBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="options_orderlines")
 * @ORM\Entity(repositoryClass="eezeecommerce\OrderBundle\Entity\OptionsOrderlinesRepository")
 */
class OptionsOrderlines
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Assert\Valid
     * @ORM\ManyToOne(targetEntity="eezeecommerce\OrderBundle\Entity\OrderLines", inversedBy="options")
     * @ORM\JoinColumn(name="orderlines_id", referencedColumnName="id")
     */
    protected $orderline;

    /**
     * @Assert\Valid
     * @ORM\ManyToOne(targetEntity="eezeecommerce\ProductBundle\Entity\Options", inversedBy="orderlines", cascade={"persist"})
     * @ORM\JoinColumn(name="option_id", referencedColumnName="id")
     */
    protected $options;

    /**
     * @ORM\Column(type="text")
     */
    protected $value;

    /**
     * @ORM\ManyToMany(targetEntity="eezeecommerce\UploadBundle\Entity\Uploader", cascade={"persist"})
     * @ORM\JoinTable(name="optionsorderline_files",
     * joinColumns={@ORM\JoinColumn(name="optionsorderline_id", referencedColumnName="id")},
     * inverseJoinColumns={@ORM\JoinColumn(name="image_id", referencedColumnName="id")})
     */
    protected $file;

    /**
     * @Assert\Valid
     * @ORM\ManyToOne(targetEntity="eezeecommerce\ProductBundle\Entity\OptionChoices", inversedBy="optionsorderlines")
     * @ORM\JoinColumn(name="optionchoice_id", referencedColumnName="id")
     */
    protected $option_choice;

    /**
     * @ORM\Column(type="decimal", precision=19, scale=6)
     */
    private $option_amount;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->file = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return OptionsOrderlines
     */
    public function setValue($value)
    {
        $this->value = serialize($value);

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return unserialize($this->value);
    }

    /**
     * Set orderline
     *
     * @param \eezeecommerce\OrderBundle\Entity\OrderLines $orderline
     *
     * @return OptionsOrderlines
     */
    public function setOrderline(\eezeecommerce\OrderBundle\Entity\OrderLines $orderline = null)
    {
        $this->orderline = $orderline;

        return $this;
    }

    /**
     * Get orderline
     *
     * @return \eezeecommerce\OrderBundle\Entity\OrderLines
     */
    public function getOrderline()
    {
        return $this->orderline;
    }

    /**
     * Set options
     *
     * @param \eezeecommerce\ProductBundle\Entity\Options $options
     *
     * @return OptionsOrderlines
     */
    public function setOptions(\eezeecommerce\ProductBundle\Entity\Options $options = null)
    {
        $this->options = $options;

        return $this;
    }

    /**
     * Get options
     *
     * @return \eezeecommerce\ProductBundle\Entity\Options
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Add file
     *
     * @param \eezeecommerce\UploadBundle\Entity\Uploader $file
     *
     * @return OptionsOrderlines
     */
    public function addFile(\eezeecommerce\UploadBundle\Entity\Uploader $file)
    {
        $this->file[] = $file;

        return $this;
    }

    /**
     * Remove file
     *
     * @param \eezeecommerce\UploadBundle\Entity\Uploader $file
     */
    public function removeFile(\eezeecommerce\UploadBundle\Entity\Uploader $file)
    {
        $this->file->removeElement($file);
    }

    /**
     * Get file
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set optionChoice
     *
     * @param \eezeecommerce\ProductBundle\Entity\OptionChoices $optionChoice
     *
     * @return OptionsOrderlines
     */
    public function setOptionChoice(\eezeecommerce\ProductBundle\Entity\OptionChoices $optionChoice = null)
    {
        $this->option_choice = $optionChoice;

        return $this;
    }

    /**
     * Get optionChoice
     *
     * @return \eezeecommerce\ProductBundle\Entity\OptionChoices
     */
    public function getOptionChoice()
    {
        return $this->option_choice;
    }

    public function __toString()
    {
        return "Options";
    }

    /**
     * Set optionAmount
     *
     * @param string $optionAmount
     *
     * @return OptionsOrderlines
     */
    public function setOptionAmount($optionAmount)
    {
        $this->option_amount = $optionAmount;

        return $this;
    }

    /**
     * Get optionAmount
     *
     * @return string
     */
    public function getOptionAmount()
    {
        return $this->option_amount;
    }
}
