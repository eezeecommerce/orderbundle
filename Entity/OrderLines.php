<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace eezeecommerce\OrderBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="order_lines")
 * @ORM\Entity(repositoryClass="eezeecommerce\OrderBundle\Entity\OrderLinesRepository")
 */
class OrderLines
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="integer")
     */
    protected $qty;
    
    /**
     * @ORM\ManyToOne(targetEntity="eezeecommerce\OrderBundle\Entity\Orders", inversedBy="order_lines", cascade={"persist"})
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     */
    private $order;

    /**
     * @ORM\ManyToOne(targetEntity="eezeecommerce\ProductBundle\Entity\Product", inversedBy="orderlines", cascade={"persist"})
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     */
    private $product;

    /**
     * @ORM\ManyToOne(targetEntity="eezeecommerce\ProductBundle\Entity\Variants", inversedBy="orderlines")
     * @ORM\JoinColumn(name="variant_id", referencedColumnName="id")
     */
    private $variant;

    /**
     * @Assert\Valid
     * @ORM\OneToMany(targetEntity="eezeecommerce\OrderBundle\Entity\OptionsOrderlines", mappedBy="orderline", cascade={"persist"})
     */
    private $options;

    /**
     * @ORM\Column(type="decimal", precision=19, scale=6)
     */
    private $product_amount;

    /**
     * @ORM\Column(type="decimal", precision=19, scale=6)
     */
    private $total_amount;


    public function __construct()
    {
        $this->options = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set qty
     *
     * @param integer $qty
     *
     * @return OrderLines
     */
    public function setQty($qty)
    {
        $this->qty = $qty;

        return $this;
    }

    /**
     * Get qty
     *
     * @return integer
     */
    public function getQty()
    {
        return $this->qty;
    }

    /**
     * Set order
     *
     * @param \eezeecommerce\OrderBundle\Entity\Orders $order
     *
     * @return OrderLines
     */
    public function setOrder(\eezeecommerce\OrderBundle\Entity\Orders $order = null)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return \eezeecommerce\OrderBundle\Entity\Orders
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set product
     *
     * @param \eezeecommerce\ProductBundle\Entity\Product $product
     *
     * @return OrderLines
     */
    public function setProduct(\eezeecommerce\ProductBundle\Entity\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \eezeecommerce\ProductBundle\Entity\Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set variant
     *
     * @param \eezeecommerce\ProductBundle\Entity\Variants $variant
     *
     * @return OrderLines
     */
    public function setVariant(\eezeecommerce\ProductBundle\Entity\Variants $variant = null)
    {
        $this->variant = $variant;

        return $this;
    }

    /**
     * Get variant
     *
     * @return \eezeecommerce\ProductBundle\Entity\Variants
     */
    public function getVariant()
    {
        return $this->variant;
    }

    /**
     * Add option
     *
     * @param \eezeecommerce\OrderBundle\Entity\OptionsOrderlines $option
     *
     * @return OrderLines
     */
    public function addOption(\eezeecommerce\OrderBundle\Entity\OptionsOrderlines $option)
    {
        $this->options[] = $option;

        $option->setOrderline($this);

        return $this;
    }

    /**
     * Remove option
     *
     * @param \eezeecommerce\OrderBundle\Entity\OptionsOrderlines $option
     */
    public function removeOption(\eezeecommerce\OrderBundle\Entity\OptionsOrderlines $option)
    {
        $this->options->removeElement($option);
    }

    /**
     * Get options
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Set productAmount
     *
     * @param string $productAmount
     *
     * @return OrderLines
     */
    public function setProductAmount($productAmount)
    {
        $this->product_amount = $productAmount;

        return $this;
    }

    /**
     * Get productAmount
     *
     * @return string
     */
    public function getProductAmount()
    {
        return $this->product_amount;
    }

    /**
     * Set totalAmount
     *
     * @param string $totalAmount
     *
     * @return OrderLines
     */
    public function setTotalAmount($totalAmount)
    {
        $this->total_amount = $totalAmount;

        return $this;
    }

    /**
     * Get totalAmount
     *
     * @return string
     */
    public function getTotalAmount()
    {
        return $this->total_amount;
    }
}
