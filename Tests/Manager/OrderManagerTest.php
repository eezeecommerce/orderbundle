<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 23/05/16
 * Time: 10:31
 */

namespace eezeecommerce\OrderBundle\Tests\Manager;


use eezeecommerce\CartBundle\Core\CartManager;
use eezeecommerce\OrderBundle\Manager\OrderManager;
use eezeecommerce\OrderBundle\Manager\OrderManagerInterface;
use eezeecommerce\OrderBundle\Order\OrderItem;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use eezeecommerce\CartBundle\Storage\StorageInterface;
use eezeecommerce\OrderBundle\Entity\Orders;
use eezeecommerce\CartBundle\Storage\SessionStorage;
use eezeecommerce\TaxBundle\Calculator\TaxCalculator;

class OrderManagerTest extends WebTestCase
{
    /**
     * @var Client
     */
    private $client = null;

    public function setUp()
    {
        $this->client = static::createClient();
    }
    public function testOrderManagerImplementsOrderManagerInterface()
    {
        $manager = new \ReflectionClass(OrderManager::class);

        $this->assertTrue($manager->implementsInterface(OrderManagerInterface::class));
    }

    public function testOrderManagerRequiresTwoArguments()
    {
        $eventDispatcher = $this->getMockBuilder(EventDispatcherInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $storage = $this->getMockBuilder(StorageInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $manager = new OrderManager($eventDispatcher, $storage);

        $this->assertNotFalse($manager);
    }

    public function testOrderManagerCreatesOrderInStorage()
    {
        $session = $this->client->getContainer()->get("session");

        $dispatcher = $this->getMockBuilder("Symfony\Component\EventDispatcher\EventDispatcherInterface")
            ->disableOriginalConstructor()
            ->getMock();

        $storage = new SessionStorage($session);

        $entity = new Orders();

        $manager = new OrderManager($dispatcher, $storage);

        $manager->setOrder($entity);

        $expected = new OrderItem($entity);

        $this->assertEquals($expected, $manager->getOrder());
    }

    public function testOrderManagerReturnsEmpty()
    {
        $session = $this->client->getContainer()->get("session");

        $dispatcher = $this->getMockBuilder("Symfony\Component\EventDispatcher\EventDispatcherInterface")
            ->disableOriginalConstructor()
            ->getMock();

        $storage = new SessionStorage($session);

        $entity = new Orders();

        $manager = new OrderManager($dispatcher, $storage);

        $this->assertEquals(0, $manager->count());

        $this->assertTrue($manager->isEmpty());

        $manager->setOrder($entity);

        $this->assertEquals(1, $manager->count());

        $this->assertNotTrue($manager->isEmpty());
    }

    public function testRemoveOrderRemovesOrderInstance()
    {
        $session = $this->client->getContainer()->get("session");

        $dispatcher = $this->getMockBuilder("Symfony\Component\EventDispatcher\EventDispatcherInterface")
            ->disableOriginalConstructor()
            ->getMock();

        $storage = new SessionStorage($session);

        $entity = new Orders();

        $manager = new OrderManager($dispatcher, $storage);

        $manager->setOrder($entity);

        $this->assertEquals(1, $manager->count());

        $manager->clear();

        $this->assertEquals(0, $manager->count());
    }

    public function testOrderManagerAllowsCartToBeSet()
    {
        $session = $this->client->getContainer()->get("session");

        $dispatcher = $this->getMockBuilder("Symfony\Component\EventDispatcher\EventDispatcherInterface")
            ->disableOriginalConstructor()
            ->getMock();

        $storage = new SessionStorage($session);

        $cart = $this->getMockBuilder(CartManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        $manager = new OrderManager($dispatcher, $storage);

        $manager->setCart($cart);

        $this->assertEquals($cart, $manager->getCart());

    }

    public function testHasOrderReturnsTrue()
    {
        $session = $this->client->getContainer()->get("session");

        $dispatcher = $this->getMockBuilder("Symfony\Component\EventDispatcher\EventDispatcherInterface")
            ->disableOriginalConstructor()
            ->getMock();

        $storage = new SessionStorage($session);

        $entity = new Orders();

        $manager = new OrderManager($dispatcher, $storage);

        $this->assertFalse($manager->hasOrder());

        $manager->setOrder($entity);

        $this->assertTrue($manager->hasOrder());
    }

}