<?php

namespace eezeecommerce\OrderBundle\Tests\Manager;


use Doctrine\Common\Persistence\ObjectManager;
use eezeecommerce\CartBundle\Core\CartManager;
use eezeecommerce\OrderBundle\Entity\Orders;
use eezeecommerce\OrderBundle\Manager\OrderProcessor;

class OrderProcessorTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var ObjectManager $om
     */
    protected $om;

    /**
     * @var CartManager $cart
     */
    protected $cart;

    public function setUp()
    {
        $this->cart = $this->getMockBuilder(CartManager::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->om = $this->getMockBuilder(ObjectManager::class)
            ->disableOriginalConstructor()
            ->getMock();
    }

    public function testConstructorAcceptsTwoArguments()
    {
        new OrderProcessor($this->om, $this->cart);
    }


    /**
     * @expectedException \InvalidArgumentException
     */
    public function testSetOrderOnlyAcceptsOrdersEntity()
    {
        $processor = new OrderProcessor($this->om, $this->cart);
        $processor->setOrder(new \stdClass());
    }
}