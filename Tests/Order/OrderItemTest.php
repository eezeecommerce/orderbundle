<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 23/05/16
 * Time: 11:28
 */

namespace eezeecommerce\OrderBundle\Tests\Order;


use eezeecommerce\OrderBundle\Order\OrderItem;
use eezeecommerce\OrderBundle\Entity\Orders;

class OrderItemTest extends \PHPUnit_Framework_TestCase
{
    public function testConstructRequiresInstanceOfOrder()
    {
        $order = new Orders();

        $item = new OrderItem($order);

        $this->assertEquals($order, $item->getEntity());
    }
}