<?php

namespace eezeecommerce\OrderBundle\Tests\Event;

use eezeecommerce\OrderBundle\Event\OrderEvent;
use Symfony\Component\EventDispatcher\Event;
use eezeecommerce\OrderBundle\Order\OrderItem;


class OrderEventTest extends \PHPUnit_Framework_TestCase
{
    public function testOrderEventExtendsEvent()
    {
        $stub = new \ReflectionClass(OrderEvent::class);

        $this->assertTrue($stub->isSubclassOf(Event::class));
    }

    public function testOrderEventReturnsInstanceOfOrderItem()
    {
        $orderItem = $this->getMockBuilder(OrderItem::class)
            ->disableOriginalConstructor()
            ->getMock();

        $event = new OrderEvent($orderItem);

        $this->assertEquals($orderItem, $event->getOrder());
    }
}