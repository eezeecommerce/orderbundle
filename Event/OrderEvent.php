<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 23/05/16
 * Time: 09:37
 */

namespace eezeecommerce\OrderBundle\Event;

use eezeecommerce\OrderBundle\Order\OrderItem;
use Symfony\Component\EventDispatcher\Event;

class OrderEvent extends Event
{

    protected $order;


    public function __construct(OrderItem $order)
    {
        $this->order = $order;
    }

    public function getOrder()
    {
        return $this->order;
    }
}