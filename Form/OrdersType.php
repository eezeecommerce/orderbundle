<?php

namespace eezeecommerce\OrderBundle\Form;

use eezeecommerce\UserBundle\Form\AddressType;
use eezeecommerce\UserBundle\Form\ShippingAddressType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrdersType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("address_option", ChoiceType::class, array(
                "choices" => array(
                    0 => "I am shipping to the same address as my billing address",
                    1 => "I am shipping to a different address"
                ),
                "mapped" => false,
                "label" => false,
            ))
            ->add("billing_address", AddressType::class)
            ->add('vat_number', null, array(
                "label" => false,
            ))
            ->add("shipping_address", new ShippingAddressType(), array(
                "required" => false
            ))
            ->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
                $data = $event->getData();
                $form = $event->getForm();

                $addressOption = (int)$form["address_option"]->getData();

                if ($addressOption === 1) {
                    $data->getShippingAddress()->setContactEmail(
                        $data->getBillingAddress()->getContactEmail()
                    );
                    $data->getShippingAddress()->setContactPhone(
                        $data->getBillingAddress()->getContactPhone()
                    );
                }
                else {
                    $data->setShippingAddress($data->getBillingAddress());
                }
            })
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'eezeecommerce\OrderBundle\Entity\Orders'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'eezeecommerce_orderbundle_orders';
    }
}
